<?php


namespace Yeltrik\ImportAsana\app\import;


use Illuminate\Http\Request;

class Abstract_ImportAsanaJson extends Abstract_ImportAsana
{

    private array $json;

    /**
     * Abstract_ImportAsanaJson constructor.
     * @param Request $request
     * @param array $json
     */
    public function __construct(Request $request, array $json)
    {
        parent::__construct($request);
        $this->json = $json;
    }

    /**
     * @return array
     */
    public function json()
    {
        return $this->json;
    }

}
