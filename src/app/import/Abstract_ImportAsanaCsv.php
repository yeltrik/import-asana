<?php


namespace Yeltrik\ImportAsana\app\import;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Abstract_ImportAsanaCsv extends Abstract_ImportAsana
{
    const FILE_PROPERTY_NAME = "asana_export_csv_file";

    private array $csv;

    /**
     * Abstract_ImportAsanaCsv constructor.
     * @param Request $request
     * @param array $csv
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->csv = $this->initCsv($request);
    }

    /**
     * @return array
     */
    public function csv()
    {
        return $this->csv;
    }

    /**
     * @param Request $request
     * @return array
     * @throws FileNotFoundException
     */
    public function initCsv(Request $request)
    {
        $propertyName = static::FILE_PROPERTY_NAME;
        $fileName = time() . '_' . $request->$propertyName->getClientOriginalName();
        $filePath = $request->file(static::FILE_PROPERTY_NAME)->storeAs('uploads', $fileName, 'public');

        $data = Storage::disk('public')->get($filePath);
        $bom = pack('CCC', 0xEF, 0xBB, 0xBF);
        if (strncmp($data, $bom, 3) === 0) {
            $data = substr($data, 3);
        }
        $rows = explode("\n", $data);
        return $this->associateCsv($rows);
    }

    private function associateCsv($rows)
    {
        $array = array_map('str_getcsv', $rows);
        array_walk($array, function (&$a) use ($array) {
            if (sizeof($array[0]) === sizeof($a)) {
                $a = array_combine($array[0], $a);
            }
        });
        array_shift($array); # remove column header
        return $array;
    }

}
