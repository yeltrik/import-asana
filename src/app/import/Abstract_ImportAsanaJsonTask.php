<?php


namespace Yeltrik\ImportAsana\app\import;


use Illuminate\Http\Request;

class Abstract_ImportAsanaJsonTask extends Abstract_ImportAsana
{

    private array $task;

    /**
     * Abstract_ImportAsanaJson constructor.
     * @param Request $request
     * @param array $task
     */
    public function __construct(Request $request, array $task)
    {
        parent::__construct($request);
        $this->task = $task;
    }

    /**
     * @return array
     */
    public function task()
    {
        return $this->task;
    }

}
