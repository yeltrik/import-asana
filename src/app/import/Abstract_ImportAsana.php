<?php


namespace Yeltrik\ImportAsana\app\import;


use Illuminate\Http\Request;

class Abstract_ImportAsana
{
    private Request $request;

    /**
     * Abstract_ImportAsana constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function request()
    {
        return $this->request;
    }

}
